# Managing continers with systemd

.footer: created By Alex M. Schapelle, VaioLabs.IO

---
# About Me


---
# Linux and Systemd 

`Linux` , or GNU/Linux is a nickname to family of open-source Unix-like operating systems based on the Linux kernel, an operating system kernel first released on September 17, 1991, by Linus Torvalds. Linux is typically packaged in a Linux distribution. 

`systemd` is a software suite that provides an array of system components for Linux operating systems. Its main aim is to unify service configuration and behavior across Linux distributions; `systemd`'s primary component is a "system and service manager"—an init system used to bootstrap user space and manage user processes. It also provides replacements for various daemons and utilities, including device management, login management, network connection management, and event logging. The name systemd adheres to the Unix convention of naming daemons by appending the letter d. It also plays on the term `SystemD`, which refers to a person's ability to adapt quickly and improvise to solve problems.

Since 2015, the majority of Linux distributions have adopted `systemd`, having replaced other systems such as the UNIX System V and BSD init systems. `systemd` has faced mixed reception from Linux users, with arguments that `systemd` suffers from mission creep and bloat, as well as criticism over software (such as the GNOME desktop) adding dependencies on systemd—complicating compatibility with other Unix-like operating systems, and making it hard to move away from `systemd`. Concerns have also been raised about Red Hat and its parent company IBM controlling the scene of init systems on Linux.

---

# Containers

- lxc: the bastard that started it all.
- docker: other bastard that took it and ran with it.
- podman: another one who said that he can do it better.


---
